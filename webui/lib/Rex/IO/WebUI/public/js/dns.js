/**
 * DNS functions
 */

(function() {

  $(document).ready(function() {

    $("a.dns-link").click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      var tld = this.getAttribute("tld");
      list_dns(tld);
    });

  });


})();

function list_dns(tld) {

  load_page("/dns/" + tld, null, function() {
    var minus_height = 260;

    // do something after loading...
    $("#table_entries tbody tr").click( function( e ) {
      if ( $(this).hasClass('row_selected') ) {
        $(this).removeClass('row_selected');
      }
      else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
      }
    });

    var oTable = $("#table_entries").dataTable({
      "bJQueryUI": true,
      "bPaginate": false,
      "sScrollY": $("#content_area").height()-minus_height,
      "sPaginationType": "full_numbers"
    });

    prepare_data_tables();

    $(window).on("resize", function() {
      if(typeof resize_Timer != "undefined") {
        window.clearTimeout(resize_Timer);
      }
      resize_Timer = window.setTimeout(function() {
        $("#table_entries").parent().css("height", $("#content_area").height()-minus_height);
        oTable.fnDraw();
      }, 200);
    });

    window.setTimeout(function() {
      $("#table_entries").parent().css("height", $("#content_area").height()-minus_height);
      oTable.fnDraw();
    }, 200);

    $("#add_dns_entry").dialog({
      autoOpen: false,
      height: 500,
      width: 350,
      modal: true,
      buttons: {
        "Add": function() {
          $.log("adding");

          add_dns_entry({
            domain: $("#domain").val(),
            type: $("#type").val(),
            name: $("#name").val(),
            data: $("#data").val(),
            ttl: $("#ttl").val(),
            callback: function() {
              list_dns($("#domain").val());
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      },
      close: function() {
        $("INPUT").val("").removeClass("ui-state-error");
        $("#ttl").val("3600");
      }
    });

    $("#dns_add_entry").click(function() {
      $("#add_dns_entry").dialog("open");
    });

    $("#dns_del_entry").click(function() {
      var selected_row = fnGetSelected(oTable);
      var domain = $(selected_row).attr("attr_domain");
      var type = $(selected_row).attr("attr_type");
      var name = $(selected_row).attr("attr_name");

      delete_dns_entry(domain, name, type);
    });


  });


}

function delete_dns_entry(domain, host, type) {
  dialog_confirm({
    id: "dns_delete_dialog",
    title: "Really delete " + host,
    text: "This entry will be permanently deleted and cannot be recovered. Are you sure?",
    button: "Delete",
    ok: function() {

      rexio.call(
        "DELETE",
        "1.0",
        "dns",
        ["domain", domain, "entry", host, "type", type],
        function(data) {
          $.pnotify(
            {
              "title": "DNS entry deleted",
              "text" : "DNS entry '" + host + "' (" + type + ") removed from domain '" + domain + "'",
              "type" : "info"
            }
          );

          list_dns(domain);
        }
      );
    },
    cancel: function() {}
  });
}

/*

CALL:

add_dns_entry({
  domain: "foo.bar",
  type: "A",
  name: "fe01",
  data: "192.168.7.2",
  ttl: 3600
});

*/
function add_dns_entry(ref) {

  rexio.call(
    "POST",
    "1.0",
    "dns",
    [
      "domain", ref.domain,
      "entry" , ref.name,
      "type"  , ref.type,
      "ref"   , {
        "data"   : ref.data,
        "ttl"    : ref.ttl,
        "base64" : ($("#base64").attr("checked") ? 1 : 0)
      }
    ],
    function(data) {
      if(data.ok != true) {
        $.pnotify(
          {
            "title": "Error adding new DNS entry",
            "text" : "Can't add '" + ref.name + "' ('" + ref.type +"') to domain '" + ref.domain + "'",
            "type" : "error"
          }
        );
      }
      else {
        $.pnotify(
          {
            "title": "New DNS entry added",
            "text" : "Added new entry '" + ref.name + "' ('" + ref.type +"') to domain '" + ref.domain + "'",
            "type" : "info"
          }
        );

        if(typeof ref.callback != "undefined") {
          ref.callback();
        }
      }
    }
  );

}
