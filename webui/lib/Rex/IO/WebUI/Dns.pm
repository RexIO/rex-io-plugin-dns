package Rex::IO::WebUI::Dns;
use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use MIME::Base64;
use File::Basename;

# This action will render a template
sub show_tld {
  my ($self) = @_;
  $self->render;
}

sub add_record {
  my ($self) = @_;

  my $option = $self->req->json;

  my $domain = $self->param("domain");
  my $entry  = $self->param("host");
  my $type   = $self->param("type");

  $self->app->log->debug(
    "Creating new dns record for $domain / $entry / $type");
  $self->app->log->debug( Dumper($option) );

  if ( $option->{data}->{base64} == 1 ) {
    $option->{data}->{data} = encode_base64 $option->{data}->{data};
    delete $option->{data}->{base64};
  }

  my $ret = $self->rexio->call(
    "POST", "1.0", "dns",
    domain => $domain,
    entry  => $entry,
    type   => $type,
    ref    => {
      ttl => $option->{data}->{ttl} || 86000,
      data => $option->{data}->{data},
    },
  );

  $self->render( json => $ret );
}

sub del_record {
  my ($self) = @_;

  my $domain = $self->param("domain");
  my $type   = $self->param("type");
  my $entry  = $self->param("host");

  $self->app->log->debug("Deleting dns entry: $domain / $entry / $type");

  my $ret = $self->rexio->call(
    "DELETE", "1.0", "dns",
    domain => $domain,
    entry  => $entry,
    type   => $type,
  );

  $self->render( json => $ret );
}

##### Rex.IO WebUI Plugin specific methods
sub __register__ {
  my ( $self, $opt ) = @_;
  my $r      = $opt->{route};
  my $r_auth = $opt->{route_auth};
  my $app    = $opt->{app};

  $r_auth->get("/dns/#tld")->to("dns#show_tld");

  $r_auth->post("/1.0/dns/domain/#domain/entry/#host/type/#type")
    ->to("dns#add_record");
  $r_auth->delete("/1.0/dns/domain/#domain/entry/#host/type/#type")
    ->to("dns#del_record");

  # add plugin template path
  push( @{ $app->renderer->paths }, dirname(__FILE__) . "/templates" );
  push( @{ $app->static->paths },   dirname(__FILE__) . "/public" );
}

1;
