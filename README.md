# DNS Plugin for Rex.IO

With this plugin it is possible to control Bind DNS Server with dynamic updates.

## API

List managed domains:

```
curl -D- -XGET \
  http://user:password@localhost:5000/1.0/dns
```

List domain entries:

```
curl -D- -XGET \
  http://user:password@localhost:5000/1.0/dns/domain/domain.tld
```

Add entry to domain:

```javascript
{
  "data": "1.2.3.4",
  "ttl" : 3600
}
```

```
curl -D- -XPOST -d@entry.json \
  http://user:password@localhost:5000/1.0/dns/domain/$domain/entry/$entry/type/$type
```

Delete an entry from a domain:

```
curl -D- -XDELETE \
  http://user:password@localhost:5000/1.0/dns/domain/$domain/entry/$entry/type/$type
```
