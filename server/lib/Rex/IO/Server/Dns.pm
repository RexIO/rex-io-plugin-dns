#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Server::Dns;
use Mojo::Base 'Mojolicious::Controller';

use Net::DNS;
use Data::Dumper;
use Mojo::JSON "j";

sub list_domain {
  my ($self) = @_;

  my $domain = $self->param("domain");
  my @ret;

  for my $rr ( $self->_dns->axfr($domain) ) {
    if ( $rr->type eq "A" ) {
      push @ret,
        {
        data => $rr->address,
        ttl  => $rr->ttl,
        type => $rr->type,
        name => $rr->name,
        };
    }
    elsif ( $rr->type eq "TXT" ) {
      push @ret,
        {
        data => $rr->rdata,
        ttl  => $rr->ttl,
        type => $rr->type,
        name => $rr->name,
        };
    }
    elsif ( $rr->type eq "CNAME" ) {
      push @ret,
        {
        data => $rr->cname,
        ttl  => $rr->ttl,
        type => $rr->type,
        name => $rr->name,
        };
    }
    elsif ( $rr->type eq "MX" ) {
      push @ret,
        {
        data => $rr->exchange,
        ttl  => $rr->ttl,
        type => $rr->type,
        name => $rr->name,
        };
    }
    elsif ( $rr->type eq "PTR" ) {
      push @ret,
        {
        data => $rr->ptrdname,
        ttl  => $rr->ttl,
        type => $rr->type,
        name => $rr->name,
        };
    }
    else {
      $self->app->log->debug( Dumper($rr) );
    }
  }

  $self->render( json => { ok => Mojo::JSON->true, data => \@ret } );
}

sub list_tlds {
  my ($self) = @_;

  my @ret = ();

  for my $tld ( @{ $self->config->{dns}->{tlds} } ) {
    if ( ref $tld eq "HASH" ) {
      push @ret, $tld;
    }
    else {
      push @ret,
        {
        zone => $tld,
        name => $tld,
        };
    }
  }

  $self->render( json => { ok => Mojo::JSON->true, data => \@ret } );
}

sub get {
  my ($self) = @_;

  my $domain = $self->param("domain");
  my $host   = $self->param("host");

  my $query = $self->_dns->search("$host.$domain");

  my $ret = {};

  foreach my $rr ( $query->answer ) {
    $ret->{ $rr->name } = {
      ip   => $rr->address,
      ttl  => $rr->ttl,
      name => $rr->name,
      type => $rr->type,
    };
  }

  $self->render( json => { ok => Mojo::JSON->true, data => $ret } );
}

# CALL:
# curl -X POST -d '{"data":"1.2.3.4","type":"A"}' http://localhost:5000/1.0/dns/stage.rexify.org/entry/fe01
# curl -X POST -d '{"data":"fe01","type":"PTR"}' http://localhost:5000/1.0/dns/4.3.2.IN-ADDR.ARPA/entry/1
sub add_record {
  my ($self) = @_;

  my $domain      = $self->param("domain");
  my $host        = $self->param("host");
  my $record_type = $self->param("type");

  my $update = Net::DNS::Update->new($domain);

  my $json = $self->req->json;

  my $ttl = $json->{ttl} || "86400";
  my $data = $json->{data};

  $self->app->log->debug(
    "Adding new dns entry: $host.$domain. $ttl $record_type $data");

  $self->app->log->debug(Dumper($json));

#if(! $self->_is_ip($ip)) {
#  return $self->render(json => {ok => Mojo::JSON->false, error => "Not a valid IPv4 given."}, status => 500);
#}

#if(! $self->_is_hostname($host)) {
#  return $self->render(json => {ok => Mojo::JSON->false, error => "Not a valid HOSTNAME given."}, status => 500);
#}

  # don't add it, if there is already an A record
  $update->push( prerequisite => nxrrset("$host.$domain. $record_type") );

  if ( $record_type eq "TXT" ) {
    $data =~ s/\\/\\\\/gms;
    $data =~ s/"/\\"/gms;
    $data = "\"$data\"";
  }

  $update->push( update => rr_add("$host.$domain. $ttl $record_type $data") );

  $update->sign_tsig( $self->config->{dns}->{key_name},
    $self->config->{dns}->{key} );

  my $res   = $self->_dns;
  my $reply = $res->send($update);

  if ($reply) {
    my $rcode = $reply->header->rcode;

    if ( $rcode eq "NOERROR" ) {
      $self->app->log->debug("DNS Entry added.");
      return $self->render( json => { ok => Mojo::JSON->true } );
    }
    else {
      $self->app->log->error("Error adding new dns entry: $rcode");
      return $self->render(
        json => {
          ok    => Mojo::JSON->false,
          error => $rcode
        },
        status => 500
      );
    }
  }
  else {
    $self->app->log->error(
      "Error adding new dns entry: " . $res->errorstring );

    return $self->render(
      json => {
        ok    => Mojo::JSON->false,
        error => $res->errorstring
      },
      status => 500
    );
  }
}

sub delete_record {
  my ($self) = @_;

  my $domain = $self->param("domain");
  my $host   = $self->param("host");
  my $type   = $self->param("type");

#if(! $self->_is_hostname($host)) {
#  return $self->render(json => {ok => Mojo::JSON->false, error => "Not a valid HOSTNAME given."}, status => 500);
#}

  $self->app->log->debug("Deleting dns entry: $domain / $type / $host");

  my $update = Net::DNS::Update->new($domain);

  $update->push( prerequisite => yxrrset("$host.$domain $type") );
  $update->push( update       => rr_del("$host.$domain $type") );

  $update->sign_tsig( $self->config->{dns}->{key_name},
    $self->config->{dns}->{key} );

  my $res   = $self->_dns;
  my $reply = $res->send($update);

  if ($reply) {
    my $rcode = $reply->header->rcode;

    if ( $rcode eq "NOERROR" ) {
      $self->app->log->debug("Successfully deleted dns entry.");
      return $self->render( json => { ok => Mojo::JSON->true } );
    }
    else {
      $self->app->log->error("Error deleting dns entry: $rcode.");
      return $self->render(
        json   => { ok => Mojo::JSON->false, error => $rcode },
        status => 500
      );
    }
  }
  else {
    $self->app->log->error( "Error deleting dns entry: " . $res->errorstring );

    return $self->render(
      json  => ok => Mojo::JSON->false,
      error => $res->errorstring
    );
  }

}

sub _dns {
  my ($self) = @_;

  my $res = Net::DNS::Resolver->new;
  $res->nameservers( $self->config->{dns}->{server} );

  return $res;
}

sub _is_ip {
  my ( $self, $ip ) = @_;

  if ( $ip =~
    m/^((25[0-5]|2[0-4]\d|1\d{2}|\d{1,2})\.){3}(25[0-5]|2[0-4]\d|1\d{2}|\d{1,2})$/
    )
  {
    return 1;
  }
}

sub _is_hostname {
  my ( $self, $hostname ) = @_;

  if ( $hostname =~ m/^([a-zA-Z0-9\-]*[a-zA-Z0-9])$/ ) {
    return 1;
  }
}

sub __register__ {
  my ( $self, $app ) = @_;
  my $r = $app->routes;

  # list entries of a domain
  $r->get("/1.0/dns/domain/#domain")->over( authenticated => 1 )
    ->to("dns#list_domain");

  # list all tlds
  $r->get("/1.0/dns")->over( authenticated => 1 )->to("dns#list_tlds");

  # add new record to domain
  $r->post("/1.0/dns/domain/#domain/entry/#host/type/#type")
    ->over( authenticated => 1 )->to("dns#add_record");

  # delete record from domain
  $r->delete("/1.0/dns/domain/#domain/entry/#host/type/#type")
    ->over( authenticated => 1 )->to("dns#delete_record");

}

1;
